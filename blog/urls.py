from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^about$', views.about, name='about'),
    url(r'^portfolio$', views.portfolio, name='portfolio'),
    url(r'^project$', views.project, name='project'),
    url(r'^posts/$', views.post_list, name='post_list'),
    url(r'^posts/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r'^projects/$', views.project_list, name='project_list'),
]

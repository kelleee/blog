from django import forms

class ContactForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'email','message')

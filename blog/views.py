from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.shortcuts import redirect
from .models import Post

# Create your views here.
def index(request):
    return render(request, 'blog/index.html', {});

def about(request):
    return render(request, 'blog/about.html', {});

def portfolio(request):
    return render(request, 'blog/portfolio.html', {});

def project(request):
    return render(request, 'blog/project_list.html', {});

def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-created_date')
    return render(request, 'blog/post_list.html', {'posts':posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

def project_list(request):
    posts = Project.objects.filter(published_date__lte=timezone.now()).order_by('-created_date')
    return render(request, 'blog/project_list.html', {'posts':posts})
